### Pop Quiz - Day 1

1. What is destructuring? Why would you use it?

- syntactic sugaring. assigning values from arrays or objects.

2. What What library renders React for the web?
   React-DOM.
3. Var is dead. Why? What should ya use instead?
   Functional programming. const. var does not respect scope.
4. How do you copy an object in JS? Is it a deep or shallow copy? What's the diff?
   defaults to shallow copy.
5. What tool did we use to generated our React app?
   npx

- npx create-react-app

6. The tool we used, uses Webpack. What's webpack?
7. What is Babel? Why do we need it?
   Babel converts newer JavaScript to older versions. Not all browsers support the same version so JS code can be generated to a least common version.
8. What tool am I using to format my code? Why is autoformatting useful?
   Prettier. Consistant format (enforces its own style) and pseudo-compiler...checking for typos.
9. What hook did we use for handling data that changes over time?
   useState
10. When does React re-render?
    Updates are queued. When time allows, a single update is made.
11. What hook did we use to run code once, when the component first loads?

- useEffect

12. We created an infinite loop yesterday with that hook. What was our mistake?
    Updates caused re-render...re-render caused updates.
13. What tool did we use to create a mock API? Why not just hardcode some json
    db.json.
14. What did we use to make an HTTP call?
    Fetch library.
15. What is the name of React's "flavor" of HTML?
    JSX
16. Name at least 3 differences between HTML and the answer to 15?
    class and className, for and htmlFor, camelcase for event handlers.
17. Is JSX required?
18. What is JSX compiled down to?
    HTML?
19. What tool is checking our code quality? Is it configurable? How do I see its output?

- eslint

20. Debugging: How do I debug my React app?
    debugger statement to break in browser.
21. I can see my original code in the browser. Is the vbrwser running my original code? If not, what technology is allowing me to see my original code?
    It is not running original code.

- source mapping. available with dev tools

22. What is a function that returns a boolean?
    Predicate.
23. Name some useful array methods that are friendly to immutable data?
    splice(), filter(), map(), every(), some()

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
