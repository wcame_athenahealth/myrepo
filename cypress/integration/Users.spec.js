context("Users", () => {
  beforeEach(() => {
    cy.visit("http://localhost:3000/users");
  });

  const testusers = [
    { name: "Bob", email: "Bob@compulink.com" },
    { name: "John Smith", email: "john.smith@compulink.com" }
  ];

  it("should add, edit and delete a user", () => {
    cy.findByText("Add User").click();
    cy.findByLabelText("Name:").type(testusers[0].name);
    cy.findByLabelText("e-mail:").type(testusers[0].email);
    cy.findByText("Cancel").click();

    cy.findByText("Add User").click();
    cy.findByLabelText("Name:").type(testusers[0].name);
    cy.findByLabelText("e-mail:").type(testusers[0].email + "{enter}");

    cy.findAllByLabelText("Edit user " + testusers[0].name).click();
    cy.findByText("Cancel").click();

    cy.findAllByLabelText("Edit user " + testusers[0].name).click();
    cy.findByLabelText("Name:").type(
      "{selectall}{backspace}" + testusers[1].name
    );
    cy.findByLabelText("e-mail:").type(
      "{selectall}{backspace}" + testusers[1].email
    );
    cy.findByText("OK").click();

    cy.findAllByLabelText("Edit user " + testusers[1].name).click();
    cy.findByText("Cancel").click();

    cy.findAllByLabelText("Delete user " + testusers[1].name).click();
  });
});
