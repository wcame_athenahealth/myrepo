// index.js
// Copyright 2010 athenahealth

import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router } from "react-router-dom";

ReactDOM.render(
  <Router>
    <App />
    {/* 3 props injected by React Router:
        Match - info on the matched URL
        Location - info on the current URL
        History - programmatically change/redirect URL
     */}
  </Router>,
  document.getElementById("root")
);
