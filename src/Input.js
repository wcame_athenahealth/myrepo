// Input.js
// Copyright 2010 athenahealth

import React from "react";
import PropTypes from "prop-types";

function Input(props) {
  return (
    <div>
      <label htmlFor={props.id}>{props.label}</label>
      <br />
      <input
        id={props.id}
        type={props.type}
        onChange={props.onChange}
        value={props.value}
      ></input>
    </div>
  );
}

// This only runs in development mode.
Input.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  type: PropTypes.oneOf(["text", "email", "number", "password"]).isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired
};

// This is ALWAYS used!
Input.defaultProps = {
  type: "text"
};

export default Input;
