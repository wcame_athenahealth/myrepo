// Input.test.js
// Copyright 2010 athenahealth

import React from "react";
import Input from "./Input";
import { render } from "@testing-library/react";

describe("Input", () => {
  it("should tie the label to the input via htmlFor", () => {
    // arrange

    // act
    const { getByLabelText, debug } = render(
      <Input id="test" label="test" type="text" onChange={() => {}} value="" />
    );

    //debug();
    // assert that the label has a value of test.
    getByLabelText("test");
  });
});
