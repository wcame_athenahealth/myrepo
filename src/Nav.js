// Nav.js
// Copyright 2010 athenahealth

import React, { useContext } from "react";
import { NavLink } from "react-router-dom";
import InternationalizationContext, {
  useTranslation,
  useTranslationNames
} from "./InternationalizationContext";

// This is a style.  No conflict.
const activeStyle = {
  color: "#614476"
};

// The Nav() function.  There will be a conflict.
function Nav() {
  const { language, setLanguage } = useContext(InternationalizationContext);
  const languagenames = useTranslationNames();
  return (
    <nav>
      <NavLink activeStyle={activeStyle} to="/" exact>
        {useTranslation("Home")}
      </NavLink>{" "}
      |{" "}
      <NavLink activeStyle={activeStyle} to="/users">
        {useTranslation("Users")}
      </NavLink>{" "}
      |{" "}
      <select
        value={language}
        onChange={event => setLanguage(event.target.value)}
      >
        {languagenames.map(name => (
          <option value={name}>{name}</option>
        ))}
      </select>
    </nav>
  );
}

export default Nav;
