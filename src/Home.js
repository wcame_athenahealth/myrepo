// Home.js
// Copyright 2010 athenahealth

import React from "react";
import { useTranslation } from "./InternationalizationContext";

function Home() {
  return (
    <>
      <h1>{useTranslation("Home")}</h1>
    </>
  );
}

export default Home;
