// InternationalizationContext.js
// Copyright 2010 athenahealth

import React, { useContext } from "react";

const InternationalizationContext = React.createContext();

// Spansish translations.
const spanish_array = [
  ["localname", "Español"],
  ["OK", "Okay"],
  ["Cancel", "Cancelar"],
  ["Name:", "Nombre:"],
  ["e-mail:", "e-mail:"],
  ["Edit User", "Editar Usuario"],
  ["Add User", "Agregar usuario"],
  ["Home", "Hogar"],
  ["Users", "Los usuarios"],
  ["ID", "Carné de identidad"],
  ["Name", "Nombre"],
  ["e-mail", "e-mail"],
  ["Delete", "Eliminar"],
  ["Edit", "Editar"]
];

// French translations.
const french_array = [
  ["localname", "Français"],
  ["OK", "Okay"],
  ["Cancel", "Annuler"],
  ["Name:", "Nom:"],
  ["e-mail:", "email:"],
  ["Edit User", "Modifier l'utilisateur"],
  ["Add User", "Ajouter un utilisateur"],
  ["Home", "Accueil"],
  ["Users", "Utilisateurs"],
  ["ID", "Identifiant"],
  ["Name", "Nom"],
  ["e-mail", "email"],
  ["Delete", "Supprimer"],
  ["Edit", "Éditer"]
];

// Array of localnames and corresponding translation map.
const langtranslations_array = [
  [spanish_array[0][1], new Map(spanish_array)],
  [french_array[0][1], new Map(french_array)]
];

// Map of all translations keyed by localname.
const langtranslations = new Map(langtranslations_array);

/**
 * function useTranslation(key)
 *
 * Using the passed key, query the current language map for that key and return the found value.
 * The key should be the default English display string.
 *
 * @param key - String used to find translation.  If not found, return key.
 */
export function useTranslation(key) {
  const { language } = useContext(InternationalizationContext);
  const translations = langtranslations.get(language);
  if (
    typeof translations !== "undefined" &&
    typeof translations.get(key) !== "undefined"
  ) {
    return translations.get(key);
  } else {
    return key;
  }
}

/**
 * function useTranslationNames()
 *
 * Return an array of local display names.
 */
export function useTranslationNames() {
  var names = [];
  names.push("English");
  langtranslations_array.forEach(element => {
    names.push(element[0]);
  });
  return names;
}

export default InternationalizationContext;
