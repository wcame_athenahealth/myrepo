// Users.js
// Copyright 2010 athenahealth

import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { useTranslation } from "./InternationalizationContext";

function Users({ users, deleteUser }) {
  /*
  { "id": 1, "name": "Billy", "email": "billcame@comcast.net" },
  { "id": 2, "name": "William", "email": "wcame@athenahealth.com" },
  { "id": 3, "name": "Will", "email": "william.came@fmr.com" },
  { "id": 4, "name": "Willy", "email": "billcame@mediaone.net" }
  */

  // h1 style.  No conflict.
  const h1Style = {
    color: "red",
    marginBottom: 20
  };

  // table style.  No conflict.
  const tableStyle = {
    border: "1px solid black",
    borderCollapse: "collapse"
  };

  const tableHdrDataStyle = { ...tableStyle, padding: "15px" };
  const deletetrans = useTranslation("Delete");
  const edittrans = useTranslation("Edit");

  // Return the React.Component for the user table but consider using Forge.  There was a conflict.
  return (
    <>
      <h1 className="header" style={h1Style}>
        {useTranslation("Users")}
      </h1>
      <Link to="/user">{useTranslation("Add User")}</Link>
      {/* Display user data in a table with headers for id, name, and e-mail */}
      <table className="table">
        <thead>
          <tr>
            <th style={tableHdrDataStyle}></th>
            <th style={tableHdrDataStyle}>{useTranslation("ID")}</th>
            <th style={tableHdrDataStyle}>{useTranslation("Name")}</th>
            <th style={tableHdrDataStyle}>{useTranslation("e-mail")}</th>
          </tr>
        </thead>
        <tbody>
          {users.map(user => (
            <tr key={user.id}>
              {/* Delay execution via arrow function */}
              <td style={{ ...tableHdrDataStyle }}>
                <button
                  aria-label={`Delete user ${user.name}`}
                  id={`delete_${user.id}`}
                  onClick={event => deleteUser(user.id)}
                >
                  {deletetrans}
                </button>
                &nbsp;
                <Link to={"/user/" + user.id}>
                  <button
                    aria-label={`Edit user ${user.name}`}
                    id={`edit_${user.id}`}
                  >
                    {/* This was not localized properly since appending is being done. */}
                    {edittrans}{" "}
                    <span aria-label="athena pencil" role="img">
                      ✏
                    </span>
                  </button>
                </Link>
              </td>
              <td style={tableHdrDataStyle}>{user.id}</td>
              <td style={tableHdrDataStyle}>{user.name}</td>
              <td style={tableHdrDataStyle}>{user.email}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
}

Users.propTypes = {
  users: PropTypes.array.isRequired,
  deleteUser: PropTypes.func.isRequired
};

export default Users;
