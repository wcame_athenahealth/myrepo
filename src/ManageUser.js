// ManageUser.js
// Copyright 2010 athenahealth

import React, { useState, useEffect } from "react";
import Input from "./Input";
import { Redirect, useRouteMatch } from "react-router-dom";
import PropTypes from "prop-types";
import { useTranslation } from "./InternationalizationContext";

const ManageUser = ({ users, addUser, editUser }) => {
  const match = useRouteMatch();
  const { userId } = match.params;
  const displayText = useTranslation(
    userId && users.length > 0 ? "Edit User" : "Add User"
  );

  const [user, setUser] = useState({ name: "", email: "" });
  const [saveCompleted, setSaveCompleted] = useState(false);

  // useEffect() runs by default after every render.
  useEffect(() => {
    if (userId && users.length > 0) {
      // Get the userId out of props.users.
      const userToEdit = users.find(user => user.id === parseInt(userId, 10));
      if (!userToEdit) return; // Todo: 404 page.
      setUser(userToEdit);
    }
  }, [userId, users]);

  function handleUserChange(event) {
    setUser({ ...user, [event.target.id]: event.target.value });
  }

  async function handleSubmit(event) {
    event.preventDefault(); // Stop browser from posting back
    userId ? await editUser(user) : await addUser(user);
    setSaveCompleted(true);
  }

  return (
    <>
      {saveCompleted && <Redirect to="/users" />}
      <h1>{displayText}</h1>
      <form onSubmit={handleSubmit}>
        <Input
          label={useTranslation("Name:")}
          id="name"
          onChange={handleUserChange}
          value={user.name}
        />
        <Input
          label={useTranslation("e-mail:")}
          id="email"
          type="email"
          onChange={handleUserChange}
          value={user.email}
        />
        <br />
        <input type="submit" value={useTranslation("OK")} />
      </form>
      <button onClick={() => window.history.back()}>
        {useTranslation("Cancel")}
      </button>
    </>
  );
};

ManageUser.propTypes = {
  users: PropTypes.array.isRequired,
  addUser: PropTypes.func.isRequired,
  editUser: PropTypes.func.isRequired
};

export default ManageUser;
