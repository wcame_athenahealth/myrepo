// utils.js
// Copyright 2010 athenahealth

export function isValidEmail(email) {
  return email.includes("@");
}
