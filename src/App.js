// App.js
// Copyright 2010 athenahealth

import React, { useState, useEffect } from "react";
import { addUser, editUser, getUsers, deleteUser } from "./api/userApi";
import Home from "./Home";
import Users from "./Users";
import ManageUser from "./ManageUser";
import Nav from "./Nav";
import { Route } from "react-router-dom";
import InternationalizationContext from "./InternationalizationContext";

function App() {
  const [users, setUsers] = useState([]);
  const [language, setLanguage] = useState("English");

  // useEffect() runs by default after every render.
  useEffect(() => {
    // Using _users to avoid naming confusion with users above.
    getUsers().then(_users => setUsers(_users));
  }, []);

  async function handleAddUser(user) {
    addUser(user).then(() => getUsers().then(_users => setUsers(_users)));
  }

  async function handleEditUser(user) {
    editUser(user).then(() => getUsers().then(_users => setUsers(_users)));
  }

  function handleDelete(id) {
    // Remove deleted element from users array
    //const newUsers = users.filter(user => user.id !== id);
    //setUsers(newUsers); // update state, so React knows to re-render.
    deleteUser(id).then(() => getUsers().then(_users => setUsers(_users)));
  }

  /* App never goes away. */
  return (
    <InternationalizationContext.Provider
      value={{ language: language, setLanguage: setLanguage }}
    >
      <Nav />
      {/* When the URL is at root, load the users component. */}
      <Route path="/" component={Home} exact />
      <Route
        path="/users"
        render={props => {
          return <Users users={users} deleteUser={handleDelete} />;
        }}
      />
      {/* "/:userId?
      If there is a "/", it is followed by an "id".  "?" means it is optional.  This is a placeholder.
      "*/}
      <Route
        path="/user/:userId?"
        render={props => {
          return (
            <ManageUser
              users={users}
              addUser={handleAddUser}
              editUser={handleEditUser}
            />
          );
        }}
      />
    </InternationalizationContext.Provider>
  );
}

export default App;
