// API proxy (make HTTP calls for ny app)

export async function getUsers() {
  return fetch("http://localhost:3001/users").then(response => {
    if (response.ok) {
      return response.json();
    }
    throw new Error("Bad network response.");
  });
}

// Challenge: Write getDelete function here.  Suggestion: google mdn use fetch

export async function deleteUser(id) {
  return fetch("http://localhost:3001/users/" + id, {
    method: "DELETE"
  }).then(response => {
    if (response.ok) {
      return response.json();
    }
    throw new Error("Bad network response.");
  });
}

export async function addUser(user) {
  return fetch("http://localhost:3001/users", {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(user)
  }).then(response => {
    if (response.ok) {
      return response.json();
    }
    throw new Error("Bad network response.");
  });
}

export async function editUser(user) {
  return fetch("http://localhost:3001/users/" + user.id, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(user)
  }).then(response => {
    if (response.ok) {
      return response.json();
    }
    throw new Error("Bad network response.");
  });
}
